﻿namespace ZombieProject.game.checkpoint
{
    public interface ICheckPoint
    {
        CheckPoint GetNextCheckPoint();
    }
}