﻿namespace ZombieProject
{
    public interface IFactory<out T1, in T2>
    {
        T1 Create(T2 t2);
    }
}