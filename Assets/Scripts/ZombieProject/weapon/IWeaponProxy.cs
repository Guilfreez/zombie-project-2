﻿namespace ZombieProject.weapon
{
    public interface IWeaponProxy
    {
        void TriggerProxy(IWeapon weapon, IWeaponProjectileLauncher weaponProjectileLauncher);
    }
}