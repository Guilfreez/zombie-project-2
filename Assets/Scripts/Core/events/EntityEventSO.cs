﻿using System;
using Core.entity;
using UnityEngine;

namespace Core.events
{
    [CreateAssetMenu(menuName = "Event/Entity Event", order = 0)]
    public class EntityEventSO : ScriptableObject
    {
        public event Action<LivingEntity, float> EntityDamageEvent;
        public event Action<LivingEntity, float, bool> EntityDamageModifiedEvent;
        public event Action<LivingEntity> EntityDeathEvent;

        public void OnEntityDamageEvent(LivingEntity livingEntity, float damage)
        {
            EntityDamageEvent?.Invoke(livingEntity, damage);
        }

        public void OnEntityDamageModifiedEvent(LivingEntity livingEntity, float damage, bool isDamageModified)
        {
            EntityDamageModifiedEvent?.Invoke(livingEntity, damage, isDamageModified);
        }

        public void OnEntityDeathEvent(LivingEntity livingEntity)
        {
            EntityDeathEvent?.Invoke(livingEntity);
        }
    }
}