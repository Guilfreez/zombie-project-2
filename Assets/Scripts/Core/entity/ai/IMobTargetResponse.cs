﻿namespace Core.entity.ai
{
    public interface IMobTargetResponse
    {
        void OnMobTarget(Mob mob, LivingEntity target);
        void OnMobTargetClear(Mob mob);
    }
}