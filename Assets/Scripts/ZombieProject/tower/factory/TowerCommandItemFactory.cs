﻿using System.Collections.Generic;
using UnityEngine;
using ZombieProject.tower.inventory.item;

namespace ZombieProject.tower.factory
{
    public class TowerCommandItemFactory : MonoBehaviour, ITowerCommandItemFactory
    {
        public TowerCommandItem Create(TowerSo towerSo)
        {
            var towerCommandItem = new TowerCommandItem(towerSo.enumValue, towerSo);
            var itemMeta = towerCommandItem.GetItemMeta();
            itemMeta.SetDisplayName(towerSo.displayName);
            itemMeta.SetLore(new List<string>(towerSo.description));
            itemMeta.SetAmount(towerSo.price);
            return towerCommandItem;
        }
    }
}