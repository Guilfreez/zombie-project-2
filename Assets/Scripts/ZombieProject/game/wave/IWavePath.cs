﻿using ZombieProject.game.checkpoint;

namespace ZombieProject.game.wave
{
    public interface IWavePath
    {
        CheckPoint GetStartPoint(EntranceType entranceType, PathType pathType);
    }
}