﻿using Core.events;
using UnityEngine;

namespace Core.inventory.InventoryView
{
    public class ToolTipController : MonoBehaviour
    {
        [SerializeField] private InventoryEventSO inventoryEventSo;
        [SerializeField] private Tooltip tooltip;

        private void Start()
        {
            inventoryEventSo.InventoryItemSlotEnterEvent += OnInventoryItemSlotEnterEvent;
            inventoryEventSo.InventoryItemSlotLeaveEvent += OnInventoryItemSlotLeaveEvent;
            inventoryEventSo.InventoryCloseEvent += OnInventoryCloseEvent;
        }

        private void OnDisable()
        {
            inventoryEventSo.InventoryItemSlotEnterEvent -= OnInventoryItemSlotEnterEvent;
            inventoryEventSo.InventoryItemSlotLeaveEvent -= OnInventoryItemSlotLeaveEvent;
            inventoryEventSo.InventoryCloseEvent -= OnInventoryCloseEvent;
        }

        private void OnInventoryItemSlotEnterEvent(ItemStack itemStack)
        {
            tooltip.Init(itemStack);
            tooltip.Show();
        }

        private void OnInventoryItemSlotLeaveEvent()
        {
            tooltip.Hide();
        }

        private void OnInventoryCloseEvent()
        {
            tooltip.Hide();
        }
    }
}