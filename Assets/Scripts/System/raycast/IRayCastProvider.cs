﻿namespace System.raycast
{
    public interface IRayCastProvider
    {
        IRayCastHitResponse GetRayCastHitResponse();
    }
}