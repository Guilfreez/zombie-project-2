﻿using Core.ui.gamebutton;
using Player.data;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Core.ui.mainmenu.settings
{
    public class SettingsMenu : MonoBehaviour, IVisible
    {
        [SerializeField] private UserOptionSO userOption;
        [SerializeField] private GameButton backButton;

        [Header("MUSIC BUTTON")] [SerializeField]
        private GameButton musicButton;

        [SerializeField] private Image musicImage;
        [SerializeField] private Sprite musicOnSprite;
        [SerializeField] private Sprite musicOffSprite;

        [Header("SOUND BUTTON")] [SerializeField]
        private GameButton soundButton;

        [SerializeField] private Image soundImage;
        [SerializeField] private Sprite soundOnSprite;
        [SerializeField] private Sprite soundOffSprite;

        [Header("SENSITIVITY BUTTON")] [SerializeField]
        private Slider sensitivitySlider;

        [SerializeField] private TextMeshProUGUI sensitivityText;

        protected virtual void Awake()
        {
        }

        protected virtual void Start()
        {
            backButton.SetClickAction(Hide);
            musicButton.SetClickAction(ToggleMusic);
            soundButton.SetClickAction(ToggleSound);
            sensitivitySlider.onValueChanged.AddListener(OnSensitivityChanged);

            UpdateMusicSettings();
            UpdateSoundSettings();
            UpdateSensitivitySettings();
        }

        public virtual void Show()
        {
            gameObject.SetActive(true);
        }

        public virtual void Hide()
        {
            gameObject.SetActive(false);
        }

        private void ToggleMusic()
        {
            userOption.isMusicOn = !userOption.isMusicOn;
            UpdateMusicSettings();
        }

        private void ToggleSound()
        {
            userOption.isSoundOn = !userOption.isSoundOn;
            UpdateSoundSettings();
        }

        private void OnSensitivityChanged(float value)
        {
            userOption.sensitivity = value;
            UpdateSensitivitySettings();
        }

        private void UpdateMusicSettings()
        {
            musicImage.sprite = userOption.isMusicOn ? musicOnSprite : musicOffSprite;
        }

        private void UpdateSoundSettings()
        {
            soundImage.sprite = userOption.isSoundOn ? soundOnSprite : soundOffSprite;
        }

        private void UpdateSensitivitySettings()
        {
            sensitivitySlider.value = userOption.sensitivity;

            var sensitivityDisplay = userOption.sensitivity * 100f;
            sensitivityText.text = $"Sensitivity: {sensitivityDisplay:F0}%";
        }
    }
}