﻿using Core.ui.gamebutton;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utils.scene;

namespace ZombieProject.ui.gameresult
{
    public class VictoryScreen : MonoBehaviour, IVictoryScreen
    {
        [SerializeField] private TextMeshProUGUI rewardText;
        [SerializeField] private GameButton continueButton;
        [SerializeField] private GameButton restartButton;

        private void Awake()
        {
            continueButton.SetClickAction(() => { GameSceneManager.LoadScene(GameSceneType.MAP); });
            restartButton.SetClickAction(GameSceneManager.RestartCurrentScene);
        }

        public void SetReward(int points)
        {
            rewardText.text = $"+{points} POINTS";
        }

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}