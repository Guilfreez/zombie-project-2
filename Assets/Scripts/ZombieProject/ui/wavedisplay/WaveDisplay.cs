﻿using TMPro;
using UnityEngine;

namespace ZombieProject.ui.wavedisplay
{
    public class WaveDisplay : MonoBehaviour, IWaveDisplay
    {
        [SerializeField] private TextMeshProUGUI waveText;

        private void Awake()
        {
            waveText.text = "???";
        }

        public void SetWave(int current, int maxWave)
        {
            waveText.text = $"Wave {current}/{maxWave}";
        }
    }
}