﻿using UnityEngine;

namespace Core.entity.ai
{
    public class MobAIResponse : MonoBehaviour, IMobAIResponse
    {
        public void OnMobAIUpdate(Mob mob)
        {
            if (!mob.HasTarget()) return;
            mob.Attack(mob.GetTarget());
        }
    }
}