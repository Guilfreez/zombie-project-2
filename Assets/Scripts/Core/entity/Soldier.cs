﻿using UnityEngine;
using ZombieProject.game.enemy;
using ZombieProject.game.wave;

namespace Core.entity
{
    public class Soldier : Mob
    {
        [SerializeField] private float attackDistance;
        private IWaveSpawner _waveSpawner;

        protected override void Awake()
        {
            base.Awake();
            _waveSpawner = FindObjectOfType<WaveSpawner>();
        }

        protected override void FixedUpdate()
        {
            base.FixedUpdate();
            FindTarget();
        }

        private void FindTarget()
        {
            if (HasTarget())
            {
                if (IsTargetDead(GetTarget()) || !IsInRange(GetTarget()))
                {
                    ClearTarget();
                }

                return;
            }

            foreach (var enemy in _waveSpawner.GetEnemies())
            {
                if (!IsInRange(enemy)) continue;
                if (IsTargetDead(enemy) || enemy.HasTarget()) continue;
                var enemyAi = enemy.GetComponent<EnemyAI>();
                enemyAi.Attack(this);
                SetTarget(enemy);
                break;
            }
        }

        private bool IsInRange(Entity enemy)
        {
            return Vector3.Distance(enemy.GetLocation(), GetLocation()) < attackDistance;
        }

        private bool IsTargetDead(Entity mob)
        {
            return mob.IsDead() || !mob.gameObject.activeSelf;
        }
    }
}