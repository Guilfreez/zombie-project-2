﻿namespace ZombieProject.tower.builder
{
    public interface ITowerBuilder
    {
        void Build(TowerSo newTowerSo);
    }
}