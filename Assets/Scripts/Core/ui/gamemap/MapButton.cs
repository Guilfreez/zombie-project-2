﻿using Core.ui.gamebutton;
using UnityEngine;
using Utils.scene;

namespace Core.ui.gamemap
{
    public class MapButton : GameButton
    {
        [Header("MAP INDEX")] [SerializeField] private GameSceneType levelType;

        public GameSceneType GetLevelType()
        {
            return levelType;
        }
    }
}