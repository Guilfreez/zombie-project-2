﻿using System;
using Core.entity;
using Core.events;
using Player.raycast;
using UnityEngine;
using UnityEngine.PlayerLoop;

namespace Player
{
    public class PlayerInteract : MonoBehaviour
    {
        [SerializeField] private PlayerRaycastProvider playerRaycastProvider;

        private Core.entity.Player _player;

        public void Awake()
        {
            _player = FindObjectOfType<Core.entity.Player>();
        }

        private void Start()
        {
            var heldSlot = _player.GetInventory().GetHeldItemSlot();
            EventInstance.Instance.InventoryEvent.OnPlayerItemHeldEvent(_player, heldSlot, heldSlot);
        }

        private void OnEnable()
        {
            EventInstance.Instance.InputEvent.UseItemEvent += OnUseItemEvent;
            EventInstance.Instance.InputEvent.AttackEvent += OnAttackEvent;
            EventInstance.Instance.InputEvent.PickItemEvent += OnPickItemEvent;
        }

        private void OnDisable()
        {
            EventInstance.Instance.InputEvent.UseItemEvent -= OnUseItemEvent;
            EventInstance.Instance.InputEvent.AttackEvent -= OnAttackEvent;
            EventInstance.Instance.InputEvent.PickItemEvent -= OnPickItemEvent;
        }

        private void AttackHit(RaycastHit raycastHit, bool mouseDown)
        {
            var actionType = mouseDown ? ActionType.LEFT_CLICK_DOWN : ActionType.LEFT_CLICK_UP;
            OnInteract(raycastHit, actionType);

            EventInstance.Instance.PlayerEvent.OnPlayerInteractObjectEvent(_player, raycastHit, actionType,
                _player.GetInventory().GetItemInHand());
        }

        private void AttackAir(bool mouseDown)
        {
            var actionType = mouseDown ? ActionType.LEFT_CLICK_DOWN : ActionType.LEFT_CLICK_UP;
            EventInstance.Instance.PlayerEvent.OnPlayerInteractEvent(_player, actionType,
                _player.GetInventory().GetItemInHand());
        }

        private void UseItemHit(RaycastHit raycastHit, bool mouseDown)
        {
            var actionType = mouseDown ? ActionType.RIGHT_CLICK_DOWN : ActionType.RIGHT_CLICK_UP;
            OnInteract(raycastHit, actionType);

            EventInstance.Instance.PlayerEvent.OnPlayerInteractObjectEvent(_player, raycastHit, actionType,
                _player.GetInventory().GetItemInHand());
        }

        private void UseItemAir(bool mouseDown)
        {
            var actionType = mouseDown ? ActionType.RIGHT_CLICK_DOWN : ActionType.RIGHT_CLICK_UP;
            EventInstance.Instance.PlayerEvent.OnPlayerInteractEvent(_player, actionType,
                _player.GetInventory().GetItemInHand());
        }

        private void OnPickItemEvent(bool scrollDown)
        {
            var playerInventory = _player.GetInventory();
            var heldSlot = playerInventory.GetHeldItemSlot();
            var inventorySize = playerInventory.GetSize();
            var newSlotIndex = (heldSlot + (scrollDown ? 1 : -1)) % inventorySize;
            if (newSlotIndex < 0) newSlotIndex = inventorySize - 1;
            playerInventory.SetHeldItemSlot(newSlotIndex);

            if (heldSlot != newSlotIndex)
                EventInstance.Instance.InventoryEvent.OnPlayerItemHeldEvent(_player, heldSlot, newSlotIndex);
        }

        //TODO: TEST QUICK SLOT
        private int _previousSlotIndex = 0;

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                var heldSlot = _player.GetInventory().GetHeldItemSlot();
                var newSlotIndex = 0;
                _player.GetInventory().SetHeldItemSlot(newSlotIndex);
                if (heldSlot != newSlotIndex)
                    EventInstance.Instance.InventoryEvent.OnPlayerItemHeldEvent(_player, heldSlot, newSlotIndex);
            }

            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                var heldSlot = _player.GetInventory().GetHeldItemSlot();
                var newSlotIndex = 1;
                _player.GetInventory().SetHeldItemSlot(newSlotIndex);
                if (heldSlot != newSlotIndex)
                    EventInstance.Instance.InventoryEvent.OnPlayerItemHeldEvent(_player, heldSlot, newSlotIndex);
            }

            if (Input.GetKeyDown(KeyCode.Q))
            {
                var heldSlot = _player.GetInventory().GetHeldItemSlot();
                var newSlotIndex = _previousSlotIndex;
                _previousSlotIndex = heldSlot;
                _player.GetInventory().SetHeldItemSlot(newSlotIndex);
                
                if (heldSlot != newSlotIndex)
                    EventInstance.Instance.InventoryEvent.OnPlayerItemHeldEvent(_player, heldSlot, newSlotIndex);
            }
        }


        #region RAYCASTHIT_ACTION

        private delegate void RaycastHitAction(RaycastHit raycastHit, bool mouseDown);

        private delegate void RaycastNotHitAction(bool mouseDown);

        private void DoRaycastHitAction(RaycastHitAction raycastHitAction, RaycastNotHitAction notRaycastHitAction,
            bool mouseDown)
        {
            var hit = playerRaycastProvider.GetRayCastHitResponse().GetRaycastHit();
            if (hit.collider) raycastHitAction(hit, mouseDown);
            else notRaycastHitAction(mouseDown);
        }

        private void OnAttackEvent(bool mouseDown)
        {
            DoRaycastHitAction(AttackHit, AttackAir, mouseDown);
        }

        private void OnUseItemEvent(bool mouseDown)
        {
            DoRaycastHitAction(UseItemHit, UseItemAir, mouseDown);
        }

        private void OnInteract(RaycastHit raycastHit, ActionType actionType)
        {
            if (!raycastHit.collider.TryGetComponent<IInteractable>(out var interactable)) return;
            interactable.Interact(actionType, _player);
        }

        #endregion
    }
}