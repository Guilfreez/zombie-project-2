﻿using TMPro;
using UnityEngine;

namespace ZombieProject.ui.coin
{
    public class CoinDisplay : MonoBehaviour, ICoinDisplay
    {
        [SerializeField] private TextMeshProUGUI coinAmountText;

        public void SetCoin(int previous, int current)
        {
            coinAmountText.text = $"{current}";
        }
    }
}