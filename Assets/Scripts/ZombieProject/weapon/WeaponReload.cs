﻿using UnityEngine;

namespace ZombieProject.weapon
{
    public class WeaponReload : MonoBehaviour, IWeaponReload
    {
        public void Reload(IWeapon weapon)
        {
            if (weapon.IsFull()) return;
            if (weapon.IsReloading) return;
            weapon.IsReloading = true;

            var reloadTime = weapon.GetWeaponSo().reloadTime;
            LeanTween.value(reloadTime, 0, reloadTime)
                .setOnUpdate(v => { weapon.ReloadTimeLeft = v; })
                .setOnComplete(() =>
                {
                    weapon.SetAmmo(weapon.GetMaxAmmo());
                    weapon.IsReloading = false;
                });
        }
    }
}