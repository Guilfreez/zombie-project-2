﻿namespace Core.armor
{
    public enum ArmorType
    {
        NONE, //No reduced damage
        LOW, //Reduced 1-30% damage
        MEDIUM, //Reduced 31-60% damage
        HIGH, //Reduced 61-90% damage
        GREATE, //Reduced 91+% damage
        IMMUNE, //All damage reduced
    }
}