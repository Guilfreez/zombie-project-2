﻿using Core.events;
using Core.inventory;
using Core.spectator;
using UnityEngine;

namespace Core.ui.itemBar
{
    public class ItemBarUI : BaseUI
    {
        [SerializeField] private InventoryEventSO inventoryEventSo;
        [SerializeField] private ItemBarSlot[] itemBarSlot;

        protected override void OnEnable()
        {
            base.OnEnable();
            inventoryEventSo.PlayerItemHeldEvent += PlayerItemHeldEvent;
            inventoryEventSo.PlayerInventoryUpdateEvent += OnPlayerInventoryUpdateEvent;
            EventInstance.Instance.PlayerEvent.PlayerSpectateEvent += OnPlayerSpectateEvent;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            inventoryEventSo.PlayerItemHeldEvent -= PlayerItemHeldEvent;
            inventoryEventSo.PlayerInventoryUpdateEvent -= OnPlayerInventoryUpdateEvent;
            EventInstance.Instance.PlayerEvent.PlayerSpectateEvent -= OnPlayerSpectateEvent;
        }

        private void OnPlayerInventoryUpdateEvent(IInventory inventory)
        {
            for (var i = 0; i < inventory.GetSize(); i++)
            {
                var itemStack = inventory.GetContents()[i];
                itemBarSlot[i].Init(itemStack);
            }
        }

        private void PlayerItemHeldEvent(entity.Player player, int previous, int current)
        {
            itemBarSlot[previous].SetIndicator(false);
            itemBarSlot[current].SetIndicator(true);
        }

        private void OnPlayerSpectateEvent(ISpectator spectator)
        {
            if (spectator is entity.Player) Show();
            else Hide();
        }
    }
}