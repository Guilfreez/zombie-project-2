﻿using Core.inventory;

namespace ZombieProject.weapon.inventory.item
{
    public class WeaponCommandItem : ItemStack
    {
        private readonly Weapon _weapon;

        public WeaponCommandItem(EnumValue enumValue, Weapon weapon) : base(enumValue)
        {
            _weapon = weapon;
        }

        public Weapon GetWeapon()
        {
            return _weapon;
        }
    }
}