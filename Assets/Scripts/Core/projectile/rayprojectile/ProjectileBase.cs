﻿using System.Linq;
using Core.armor;
using Core.entity;
using UnityEngine;
using Utils;

namespace Core.projectile.rayprojectile
{
    public class ProjectileBase : Entity, IProjectile
    {
        [SerializeField] private ArmorType[] bypassArmorTypes;

        protected (float damage, bool isDamageModified) GetCalculatedDamage(float damage, ArmorType armorType)
        {
            if (bypassArmorTypes.Contains(armorType) || armorType == ArmorType.NONE) return (damage, false);
            var reducedDamage = ArmorReduce.GetReducedDamage(damage, armorType);
            Debug.Log($"{armorType} | Reduced Damage from {damage} to {reducedDamage}");
            return (reducedDamage, true);
        }

        protected float Damage;
        protected ProjectileType ProjectileType;
        protected Entity Shooter;
        protected Vector3 Velocity;

        private void OnCollisionEnter(Collision other)
        {
            ProjectileInstance.Return(this);
            if (!other.transform.TryGetComponent<IDamageable>(out var damageable)) return;
            if (Shooter) damageable.Damage(GetDamage(), Shooter);
            else damageable.Damage(GetDamage());
        }

        public Entity GetShooter()
        {
            return Shooter;
        }

        public float GetDamage()
        {
            return Damage;
        }

        public ProjectileType GetProjectileType()
        {
            return ProjectileType;
        }

        public virtual void Init(Entity shooter, Transform shootTransform, float damage, Vector3 velocity,
            ProjectileType projectileType)
        {
            var projectileTransform = transform;
            projectileTransform.position = shootTransform.position;
            projectileTransform.rotation = shootTransform.rotation;

            Shooter = shooter;
            Damage = damage;
            ProjectileType = projectileType;
            Velocity = velocity;
        }
    }
}