﻿using System;
using Core.events;
using UnityEngine;
using Utils;
using ZombieProject.events;

namespace Player.controller
{
    public class GamePausedController : MonoBehaviour
    {
        private void Awake()
        {
            GamePaused.Continue();
        }

        private void OnEnable()
        {
            GameEventInstance.Instance.GameEvent.GameOverEvent += OnGameOverEvent;
            EventInstance.Instance.UIEvent.ToggleMenuEvent += OnToggleMenuEvent;
        }


        private void OnDisable()
        {
            GameEventInstance.Instance.GameEvent.GameOverEvent -= OnGameOverEvent;
            EventInstance.Instance.UIEvent.ToggleMenuEvent -= OnToggleMenuEvent;
        }

        private void OnGameOverEvent(bool win)
        {
            GamePaused.Paused();
        }

        private void OnToggleMenuEvent(bool show)
        {
            if (show) GamePaused.Paused();
            else GamePaused.Continue();
        }
    }
}