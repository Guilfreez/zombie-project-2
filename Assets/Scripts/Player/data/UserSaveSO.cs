﻿using System.Collections.Generic;
using UnityEngine;
using ZombieProject.weapon;

namespace Player.data
{
    [CreateAssetMenu(menuName = "User Save", order = 0)]
    public class UserSaveSO : ScriptableObject
    {
        public int levelUnlocked;
        public int points;
        public List<WeaponSo> purchasedItem = new List<WeaponSo>();
        public List<WeaponSo> equippedItem = new List<WeaponSo>();
    }
}