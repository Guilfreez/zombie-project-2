﻿namespace Core.entity.livingentity
{
    public interface ILivingEntityAttackResponse
    {
        void Attack(LivingEntity attacker, Entity target, float damage);
    }
}