﻿using UnityEngine;
using UnityEngine.AI;

namespace Core.entity.ai
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class MobTargetResponse : MonoBehaviour, IMobTargetResponse
    {
        private NavMeshAgent _meshAgent;

        private LivingEntity _target;

        private void Awake()
        {
            _meshAgent = GetComponent<NavMeshAgent>();
        }

        public void OnMobTarget(Mob mob, LivingEntity target)
        {
            _target = target;
            _meshAgent.SetDestination(_target.GetLocation());
        }

        public void OnMobTargetClear(Mob mob)
        {
            _target = null;
            _meshAgent.isStopped = true;
        }
    }
}