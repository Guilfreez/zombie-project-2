﻿using UnityEngine;
using Utils;
using ZombieProject.weapon;

namespace Core.entity.livingentity
{
    public class SoldierAttackResponse : MonoBehaviour, ILivingEntityAttackResponse
    {
        private Weapon _weapon;

        public void Init(Weapon weapon)
        {
            _weapon = weapon;
        }

        public void Attack(LivingEntity attacker, Entity target, float damage)
        {
            if (!(target is LivingEntity)) return;
            //Look At Target
            var dir = target.GetLocation() - attacker.GetLocation();
            dir.Normalize();
            var rotation = Quaternion.LookRotation(dir);
            attacker.transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime);
            _weapon.GetShootTransform().rotation = rotation;

            //Check FOV
            var fovToTarget = GameMath.GetTargetFOV(_weapon.GetShootTransform(), target.transform);
            if (fovToTarget > 10f) return;

            attacker.SwingHands();

            //Shoot
            if (_weapon.IsReloading) return;
            if (!_weapon.HasAmmo())
            {
                _weapon.Reload();
                return;
            }

            _weapon.Shoot();
        }
    }
}