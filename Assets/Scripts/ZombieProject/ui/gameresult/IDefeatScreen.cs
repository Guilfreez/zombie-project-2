﻿namespace ZombieProject.ui.gameresult
{
    public interface IDefeatScreen : IVisible
    {
        void Play();
    }
}