﻿using Core.ui;
using UnityEngine;
using ZombieProject.events;
using ZombieProject.game.wave;

namespace ZombieProject.ui.healthpoint
{
    public class HealthPointDisplayUI : BaseUI
    {
        [SerializeField] private GameEventSo gameEventSo;

        private IHealthPointDisplay _healthPointDisplay;

        public override void Awake()
        {
            base.Awake();
            _healthPointDisplay = GetComponent<IHealthPointDisplay>();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            gameEventSo.HealthPointChangeEvent += OnHealthPointChangeEvent;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            gameEventSo.HealthPointChangeEvent -= OnHealthPointChangeEvent;
        }

        private void OnHealthPointChangeEvent(IWaveManager waveManager, int previous, int current)
        {
            _healthPointDisplay.SetHealthPoint(previous, current);
        }
    }
}