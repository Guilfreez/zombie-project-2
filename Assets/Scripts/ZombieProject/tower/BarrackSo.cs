﻿using UnityEngine;

namespace ZombieProject.tower
{
    [CreateAssetMenu(fileName = "BarrackSo", menuName = "TD/Barrack So", order = 0)]
    public class BarrackSo : ScriptableObject
    {
        public float minDamage;
        public float maxDamage;
        public float attackSpeed;
        public float maxHealth;
    }
}