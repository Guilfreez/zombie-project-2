﻿using Core.events;
using Core.inventory;
using Core.inventory.InventoryView;
using UnityEngine;

namespace Core.entity
{
    public class HumanEntity : LivingEntity, IInventoryHolder<IPlayerInventory>
    {
        [Header("HUMAN ENTITY")] [SerializeField]
        private InventoryManager inventoryManager;

        private IInventoryView _inventoryView;

        private IPlayerInventory _playerInventory;

        protected override void Awake()
        {
            _playerInventory = new PlayerInventory(2, InventoryType.DEFAULT);
        }

        public IPlayerInventory GetInventory()
        {
            return _playerInventory;
        }

        public void OpenInventory(IInventory inventory)
        {
            _inventoryView = inventoryManager.GetInventoryView(inventory, this);
            EventInstance.Instance.InventoryEvent.OnInventoryOpenEvent(_inventoryView);
        }

        public void CloseInventory()
        {
            if (_inventoryView == null) return;
            _inventoryView.Close();
            _inventoryView = null;
            EventInstance.Instance.InventoryEvent.OnInventoryCloseEvent();
        }

        public IInventoryView GetOpenInventory()
        {
            return _inventoryView;
        }
    }
}