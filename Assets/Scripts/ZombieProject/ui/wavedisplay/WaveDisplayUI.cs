﻿using Core.ui;
using ZombieProject.events;
using ZombieProject.game.wave;

namespace ZombieProject.ui.wavedisplay
{
    public class WaveDisplayUI : BaseUI
    {
        private IWaveDisplay _waveDisplay;

        public override void Awake()
        {
            base.Awake();
            _waveDisplay = GetComponent<IWaveDisplay>();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            GameEventInstance.Instance.GameEvent.WaveStartEvent += OnWaveStartEvent;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            GameEventInstance.Instance.GameEvent.WaveStartEvent -= OnWaveStartEvent;
        }

        private void OnWaveStartEvent(IWaveManager waveManager)
        {
            _waveDisplay.SetWave(waveManager.GetWave(), waveManager.GetMaxWave());
        }
    }
}