﻿using System;
using UnityEngine;

namespace Core.ui.gamebutton
{
    public interface IGameButton
    {
        void SetImageColor(Color color);
        void SetNormalImage(Sprite sprite);
        void SetHoverImage(Sprite sprite);
        void SetClickImage(Sprite sprite);
        void SetClickAction(Action action);
    }
}