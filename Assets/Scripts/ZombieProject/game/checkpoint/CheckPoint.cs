﻿using Core.entity;
using UnityEngine;

namespace ZombieProject.game.checkpoint
{
    public class CheckPoint : LivingEntity, ICheckPoint
    {
        [SerializeField] private CheckPoint nextCheckPoint;

        protected override void Awake()
        {
            base.Awake();
            _isDead = true;
        }

        public CheckPoint GetNextCheckPoint()
        {
            return nextCheckPoint;
        }
    }
}