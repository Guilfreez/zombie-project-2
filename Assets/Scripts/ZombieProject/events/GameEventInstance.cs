﻿using Singleton;
using UnityEngine;
using ZombieProject.game.specialitem.drone.events;

namespace ZombieProject.events
{
    public class GameEventInstance : ResourceSingleton<GameEventInstance>
    {
        [SerializeField] private GameEventSo gameEventSo;
        [SerializeField] private WeaponEventSo weaponEventSo;
        [SerializeField] private SpecialItemEventSo specialItemEventSo;
        [SerializeField] private TowerEventSo towerEventSo;
        public GameEventSo GameEvent => gameEventSo;
        public WeaponEventSo WeaponEvent => weaponEventSo;
        public SpecialItemEventSo SpecialItemEvent => specialItemEventSo;
        public TowerEventSo TowerEvent => towerEventSo;
    }
}