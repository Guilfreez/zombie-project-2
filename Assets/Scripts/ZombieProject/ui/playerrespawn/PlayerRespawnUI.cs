﻿using Core.entity;
using Core.events;
using Core.ui;
using UnityEngine;
using ZombieProject.events;

namespace ZombieProject.ui.playerrespawn
{
    public class PlayerRespawnUI : BaseUI
    {
        [SerializeField] private PlayerRespawnTimer playerRespawnTimer;
        private bool _isGameOver;

        protected override void OnEnable()
        {
            base.OnEnable();
            EventInstance.Instance.PlayerEvent.PlayerHitMonsterEvent += OnPlayerHitMonsterEvent;
            GameEventInstance.Instance.GameEvent.PlayerRespawnEvent += OnPlayerRespawnEvent;
            GameEventInstance.Instance.GameEvent.PlayerRespawnTimerEvent += OnPlayerRespawnTimerEvent;
            GameEventInstance.Instance.GameEvent.GameOverEvent += OnGameOverEvent;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            EventInstance.Instance.PlayerEvent.PlayerHitMonsterEvent -= OnPlayerHitMonsterEvent;
            GameEventInstance.Instance.GameEvent.PlayerRespawnEvent -= OnPlayerRespawnEvent;
            GameEventInstance.Instance.GameEvent.PlayerRespawnTimerEvent -= OnPlayerRespawnTimerEvent;
            GameEventInstance.Instance.GameEvent.GameOverEvent -= OnGameOverEvent;
        }

        private void OnPlayerHitMonsterEvent(Monster monster)
        {
            if (_isGameOver) return;
            playerRespawnTimer.Show();
        }

        private void OnPlayerRespawnTimerEvent(float current, float delay)
        {
            playerRespawnTimer.SetRespawnTimer(current, delay);
        }

        private void OnPlayerRespawnEvent()
        {
            playerRespawnTimer.Hide();
        }

        private void OnGameOverEvent(bool win)
        {
            _isGameOver = true;
            playerRespawnTimer.Hide();
        }
    }
}