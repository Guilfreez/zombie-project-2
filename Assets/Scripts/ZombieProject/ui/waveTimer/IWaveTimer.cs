﻿namespace ZombieProject.ui.waveTimer
{
    public interface IWaveTimer
    {
        void SetWaveTimer(float current, float waveDelay);
    }
}