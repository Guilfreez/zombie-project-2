﻿using Core.ui;
using UnityEngine;
using ZombieProject.events;
using ZombieProject.game;

namespace ZombieProject.ui.gameresult
{
    public class GameResultUI : BaseUI
    {
        [SerializeField] private VictoryScreen victoryScreen;
        [SerializeField] private DefeatScreen defeatScreen;

        private WaveManager _waveManager;

        public override void Awake()
        {
            base.Awake();
            _waveManager = FindObjectOfType<WaveManager>();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            GameEventInstance.Instance.GameEvent.GameOverEvent += OnGameOverEvent;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            GameEventInstance.Instance.GameEvent.GameOverEvent -= OnGameOverEvent;
        }

        private void OnGameOverEvent(bool win)
        {
            if (win)
            {
                victoryScreen.Show();
                victoryScreen.SetReward(_waveManager.GetReward());
            }
            else
            {
                defeatScreen.Show();
                defeatScreen.Play();
            }
        }
    }
}