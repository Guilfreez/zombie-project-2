﻿using Core.entity;
using Core.events;
using Core.ui;
using UnityEngine;
using UnityEngine.UI;
using ZombieProject.events;

namespace ZombieProject.game.specialitem.drone
{
    public class DroneItemTimerUI : BaseUI
    {
        [SerializeField] private Image droneTimerImage;

        protected override void OnEnable()
        {
            base.OnEnable();
            GameEventInstance.Instance.SpecialItemEvent.DroneTimerEvent += OnDroneTimerEvent;
            EventInstance.Instance.PlayerEvent.PlayerHitMonsterEvent += OnPlayerHitMonsterEvent;
            GameEventInstance.Instance.GameEvent.PlayerRespawnEvent += OnPlayerRespawnEvent;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            GameEventInstance.Instance.SpecialItemEvent.DroneTimerEvent -= OnDroneTimerEvent;
            EventInstance.Instance.PlayerEvent.PlayerHitMonsterEvent -= OnPlayerHitMonsterEvent;
            GameEventInstance.Instance.GameEvent.PlayerRespawnEvent -= OnPlayerRespawnEvent;
        }


        private void OnDroneTimerEvent(float time, float delay)
        {
            droneTimerImage.fillAmount = Mathf.Clamp01(time / delay);
        }

        private void OnPlayerHitMonsterEvent(Monster monster)
        {
            Hide();
        }

        private void OnPlayerRespawnEvent()
        {
            Show();
        }
    }
}