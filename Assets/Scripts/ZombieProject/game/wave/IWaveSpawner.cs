﻿using System.Collections.Generic;
using Core.entity;
using ZombieProject.game.checkpoint;

namespace ZombieProject.game.wave
{
    public interface IWaveSpawner
    {
        void Init(IWaveManager waveManager);
        List<Monster> GetEnemies();
        void SpawnEnemy(EntityType entityType, CheckPoint checkPoint);
        bool IsWaveEnemy(Monster monster);
    }
}