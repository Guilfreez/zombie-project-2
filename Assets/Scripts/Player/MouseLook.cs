﻿using UnityEngine;
using Utils;

namespace Player
{
    public class MouseLook : MonoBehaviour
    {
        public Transform head;

        [Header("Mouse Settings")] public float mouseSensitivity = 1.5f;
        public float limitMouseDegree = 90f;

        private float _mouseHorizontal;
        private float _mouseVertical;

        private void Awake()
        {
            GameCursor.Hide();
        }

        private void Update()
        {
            PlayerInput();
            Look();
        }

        #region Property Player

        public float GetFovInDegree()
        {
            var dot = Vector3.Dot(transform.forward, head.forward);
            var angle = Mathf.Acos(dot);
            var degree = Mathf.Rad2Deg * angle;
            return degree;
        }

        #endregion PLAYER_PROPERTY

        private void PlayerInput()
        {
            //Mouse Look
            _mouseHorizontal = Input.GetAxis("Mouse X");
            _mouseVertical = Input.GetAxis("Mouse Y");
        }

        private void Look()
        {
            //Mouse Look
            CalculateMouse();

            transform.Rotate(Vector3.up, _mouseHorizontal * mouseSensitivity * Time.fixedDeltaTime);
            head.Rotate(Vector3.right, -_mouseVertical * mouseSensitivity * Time.fixedDeltaTime, Space.Self);
        }

        private void CalculateMouse()
        {
            var reachLimit = GetFovInDegree() > limitMouseDegree;
            if (!reachLimit) return;
            var rot = head.localEulerAngles;
            rot.x = rot.x > 90.0f ? -limitMouseDegree : limitMouseDegree;
            head.localRotation = Quaternion.Euler(rot);
        }
    }
}