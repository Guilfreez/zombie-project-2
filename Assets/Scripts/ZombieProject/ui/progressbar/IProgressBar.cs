﻿namespace ZombieProject.ui.progressbar
{
    public interface IProgressBar
    {
        void SetProgress(float current, float time);
    }
}