﻿using ZombieProject.weapon;

namespace ZombieProject.ui.weaponbar
{
    public interface IWeaponAmmoDisplay
    {
        void SetAmmo(IWeapon weapon);
    }
}