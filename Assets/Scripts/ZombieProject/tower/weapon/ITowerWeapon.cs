﻿using Core.entity;
using UnityEngine;
using ZombieProject.weapon;

namespace ZombieProject.tower.weapon
{
    public interface ITowerWeapon
    {
        Transform GetShootPoint();
        void Init(Tower tower);
        IWeapon GetWeapon();
        void Attack(LivingEntity target);
    }
}