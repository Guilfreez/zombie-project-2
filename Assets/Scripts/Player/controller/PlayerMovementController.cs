﻿using Core.events;
using Core.spectator;
using UnityEngine;

namespace Player.controller
{
    public class PlayerMovementController : MonoBehaviour
    {
        private PlayerMovement _playerMovement;

        private void Awake()
        {
            _playerMovement = FindObjectOfType<PlayerMovement>();
        }

        private void OnEnable()
        {
            EventInstance.Instance.PlayerEvent.PlayerSpectateEvent += OnPlayerSpectateEvent;
        }

        private void OnDisable()
        {
            EventInstance.Instance.PlayerEvent.PlayerSpectateEvent -= OnPlayerSpectateEvent;
        }

        private void OnPlayerSpectateEvent(ISpectator spectator)
        {
            _playerMovement.enabled = spectator is Core.entity.Player;
        }
    }
}