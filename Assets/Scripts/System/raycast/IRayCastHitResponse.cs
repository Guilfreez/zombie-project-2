﻿using UnityEngine;

namespace System.raycast
{
    public interface IRayCastHitResponse
    {
        void OnRaycastHit(RaycastHit raycastHit);
        RaycastHit GetRaycastHit();
    }
}