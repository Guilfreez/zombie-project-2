﻿using System.Collections.Generic;
using Core.entity;
using UnityEngine;

namespace Core.inventory
{
    public class InventoryManager : MonoBehaviour
    {
        [SerializeField] private InventoryView.InventoryView defaultInventoryView;

        private Dictionary<InventoryType, InventoryView.InventoryView> _inventoryViews;

        private void Awake()
        {
            defaultInventoryView.gameObject.SetActive(true);
            defaultInventoryView.gameObject.SetActive(false);

            InitInventoryView();
        }

        private void InitInventoryView()
        {
            _inventoryViews = new Dictionary<InventoryType, InventoryView.InventoryView>
            {
                {InventoryType.DEFAULT, defaultInventoryView}
            };
        }

        public InventoryView.InventoryView GetInventoryView(IInventory inventory, HumanEntity viewer)
        {
            var inventoryView = _inventoryViews[inventory.GetType()];
            inventoryView.Init(inventory, viewer);
            return inventoryView;
        }
    }
}